using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Terrian : MonoBehaviour
{
    GameObject terrian;
    public Shader shader;

    const int squares_side_length = 500;
    const int squares_count = squares_side_length * squares_side_length;
    const int vertices_side_length = (squares_side_length + 1);
    const int vertices_count = vertices_side_length * vertices_side_length;

    const int terrian_scale = 50;
    const int terrian_h_scale = 100;
    const float water_h = -0.4f * terrian_h_scale;
    const float sand_h = -0.2f * terrian_h_scale;
    const float grass_h = 0.3f * terrian_h_scale;
    const float step = 0.07f;

    OpenSimplexNoise simplex = new OpenSimplexNoise();

    void Start()
    {
        terrian = new GameObject();
        terrian.AddComponent<MeshFilter>();
        MeshRenderer r = terrian.AddComponent<MeshRenderer>();

        Mesh mesh = terrian.GetComponent<MeshFilter>().mesh;
        mesh.Clear();

        r.material.shader = shader;

        // we need that to create meshes that are more than 65535 vertices
        mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;

        Vector3[] vertices = new Vector3[vertices_count];
        Color32[] colors = new Color32[vertices_count];

        double stepx = 0.0;
        double stepy = 0.0;

        for(int y = 0; y < vertices_side_length; y++)
        {
            stepx = 0.0;
            for(int x = 0; x < vertices_side_length; x++)
            {
                float height = (float)simplex.Evaluate(stepx, stepy) * terrian_h_scale;

                if(height < water_h)
                {
                    colors[idx(x, y)] = new Color32(0, 20, 255, 255);
                }
                else if(height < sand_h)
                {
                    colors[idx(x, y)] = new Color32(230, 230, 0, 255);
                }
                else if(height < grass_h)
                {
                    colors[idx(x, y)] = new Color32(0, 230, 0, 255);
                }
                else
                {
                    colors[idx(x, y)] = new Color32(200, 200, 200, 255);
                    height *= 0.1f * height;
                }

                vertices[idx(x, y)] = new Vector3(x*terrian_scale, height, y*terrian_scale);

                stepx += step;
            }
            stepy += step;
        }

        mesh.vertices = vertices;

        mesh.colors32 = colors;

        int[] triangles = new int[(squares_count * 2) * 3];
        
        int tri_idx = 0;

        for(int j = 1; j < vertices_side_length; j++)
        {
            for(int i = 1; i < vertices_side_length; i++)
            {
                triangles[tri_idx++] =  j   *vertices_side_length + (i-1);
                triangles[tri_idx++] = (j-1)*vertices_side_length +  i;
                triangles[tri_idx++] = (j-1)*vertices_side_length + (i-1);
            }
        }   

        for(int j = 1; j < vertices_side_length; j++)
        {
            for(int i = 1; i < vertices_side_length; i++)
            {
                triangles[tri_idx++] =  j*vertices_side_length + (i-1);
                triangles[tri_idx++] =  j   *vertices_side_length +  i;
                triangles[tri_idx++] = (j-1)*vertices_side_length +  i;
            }
        } 

        mesh.triangles = triangles;

        Vector2[] uvs = new Vector2[vertices_count];

        const int uv_scale = 50;

        for (int i = 0; i < uvs.Length; i++)
        {
            uvs[i] = new Vector2(vertices[i].x/uv_scale, vertices[i].z/uv_scale);
        }

        mesh.uv = uvs;
    }

    int idx(int x, int y)
    {
        return (y * vertices_side_length) + x;
    }

    float asinb(float a, float b, float x)
    {
        return a * Mathf.Sin(b * x);
    }

    void Update()
    {

    }
}
