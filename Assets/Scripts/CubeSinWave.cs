using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CubeSinWave : MonoBehaviour
{
    const int plane_width = 50;
    const int plane_height = 50;
    GameObject[] ground_cubes;

    int idx(int x, int y)
    {
        return x + y * plane_width;
    }

    void Start()
    {
        ground_cubes = new GameObject[plane_width * plane_height];

        for(int y = 0; y < plane_height; y++)
        {
            for(int x = 0; x < plane_width; x++)
            {
                ground_cubes[idx(x, y)] = GameObject.CreatePrimitive(PrimitiveType.Cube);
                ground_cubes[idx(x, y)].transform.localScale = new Vector3(1, 1, 1);
                ground_cubes[idx(x, y)].transform.position = new Vector3(x, 0, y);
                ground_cubes[idx(x, y)].GetComponent<MeshRenderer>().material.color = new Color(0.0f, 0.2f, 0.6f, 0.1f);
            }
        }   
    }

    float step = 0.0f;

    float asinb(float a, float b, float x)
    {
        return a * Mathf.Sin(b * x);
    }

    void Update()
    {
        float[] sin_values_h = new float[plane_height];
        float[] sin_values_w = new float[plane_width];

        for(int i = 0; i < plane_height; i++)
        {
            float x = (2.0f*Mathf.PI/(float)plane_height) * ((float)i + step);
            sin_values_h[i] = asinb(0.2f, -1.7f, x) + asinb(0.6f, 0.9f, x);
        }

        for(int i = 0; i < plane_width; i++)
        {
            float x = (2.0f*Mathf.PI/(float)plane_width) * ((float)i + step);
            sin_values_w[i] = asinb(0.2f, -3.6f, x) + asinb(0.6f, 0.9f, x);
        }

        step += Time.deltaTime;

        for(int y = 0; y < plane_height; y++)
        {
            for(int x = 0; x < plane_width; x++)
            {
                ground_cubes[idx(x, y)].transform.position =
                    new Vector3(x, sin_values_h[y] + sin_values_w[x], y);
            }
        }
    }
}
